﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using MyMSACS.Models;
using Xunit;


namespace MyMSACS.Tests.Models
{
    public class StudentTest
    {
        [Fact]
        public void CreatesStudentWithGivenInformation()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                GivenName = "zzzz",
                FamilyName = "zzzz"

            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
            Assert.Equal("zzzz zzzz", model.FullName);
        }

        

        [Fact]
        public void Should_Not_Create_Student_Without_FamilyName()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                GivenName = "zzzz"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);  // should not be successful
        }

        

        [Fact]
        public void Should_Not_Create_Student_Without_GivenName()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                FamilyName = "zzzzzz"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
        }

        
        [Fact]
        public void Given_Name_must_be_short()
        {
            // Arrange
            var model = new Student()
            {
                StudentId = 1234,
                GivenName = "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY",
                FamilyName = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
            };
            var context = new ValidationContext(model, null, null);
            var result = new List<ValidationResult>();


            // Act
            var success = Validator.TryValidateObject(model, context, result, true);

            // Assert
            Assert.False(success);
            var failure = Assert.Single(result,
                x => x.ErrorMessage == "Given name cannot be longer than 50 characters.");
            Assert.Single(failure.MemberNames, x => x == "GivenName");
        }
    }
}
